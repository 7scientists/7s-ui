import { BrowserHistory, Router } from "7s/helpers/routing";
import Store from "7s/helpers/store";
import Settings from "7s/utils/settings";

const history = new BrowserHistory();
const settings = new Settings([
    ["history", history],
    ["router", new Router(history)],
    ["store", new Store()]
]);

export default settings;
