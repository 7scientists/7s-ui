import BaseActions from "7s/actions/base";

export default class Settings extends BaseActions {
    static get defaultKey() {
        return "settings";
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.set({
            status: "initialized"
        });
    }

    load() {
        const loadSettings = data => {
            const dataMap = new Map(Object.entries(data));
            // we update the settings with the external ones
            this.settings.updateWithMap(dataMap);
            this.set({ settings: dataMap, status: "loaded" });
        };

        const xhr = new XMLHttpRequest();
        xhr.open("GET", "/settings.json");

        const promise = new Promise((resolve, reject) => {
            xhr.onload = () => {
                const contentType = xhr
                    .getResponseHeader("content-type")
                    .trim();
                if (/^application\/json(;.*)?$/i.exec(contentType) === null)
                    reject({
                        status: xhr.status,
                        message: "not a JSON response",
                        errors: {}
                    });

                const data = JSON.parse(xhr.response);

                if (xhr.status >= 200 && xhr.status < 300) resolve(data);
                else {
                    reject(data);
                }
            };
            xhr.onerror = () => {
                reject({
                    status: xhr.status,
                    message:
                        xhr.statusText || this.settings.t("api.request-failed"),
                    errors: {}
                });
            };
        });
        promise.then(
            data => loadSettings(data),
            error => this.set({ status: "failed", error: error })
        );

        xhr.send();
    }
}
