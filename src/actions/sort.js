import BaseActions from "7s/actions/base";

export const ASC = "asc";
export const DESC = "desc";

export default class SortBaseActions extends BaseActions {
    static get defaultKey() {
        return "sort";
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.set({
            field: undefined,
            text: undefined,
            order: ASC
        });
    }

    updateSortField(item) {
        this.update({
            field: item.field,
            text: item.text
        });
    }

    updateSortOrder(order) {
        this.update({
            order: order
        });
    }

    resetSort() {
        this.update({
            field: undefined,
            text: undefined,
            order: ASC
        });
    }
}
