import BaseActions from "7s/actions/base";

export default class FilterBaseActions extends BaseActions {
    static get defaultKey() {
        return "filter";
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.set({ term: "" });
    }

    updateFilterTerm(term) {
        this.update({ term: term });
    }

    clearFilterTerm() {
        this.update({ term: "" });
    }
}
