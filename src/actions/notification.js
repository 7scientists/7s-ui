import BaseActions from "7s/actions/base";

export const NONE = "none";
export const PRIMARY = "primary";
export const INFO = "info";
export const SUCCESS = "success";
export const WARNING = "warning";
export const DANGER = "danger";

const uuidv4 = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (
            c ^
            (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
    );
};

export default class Notification extends BaseActions {
    static get defaultKey() {
        return "notification";
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.set({
            list: []
        });
    }

    addNotification(title, text, type = PRIMARY, timeout = 7500) {
        const { list } = this.get();
        list.push({
            id: uuidv4(),
            type: type,
            title: title,
            text: text,
            timeout: timeout
        });
        this.update({
            list: list
        });
    }

    clearNotification(id) {
        const { list } = this.get();

        const index = list.findIndex(notification => notification.id === id);

        if (index !== -1) list.splice(index, 1);

        this.update({
            list: list
        });
    }
}
