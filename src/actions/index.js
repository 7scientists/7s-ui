import Base from "./base";
import Settings from "./settings";
import Form from "./form";
import KeyValue from "./key_value";
import Title from "./title";
import Notification from "./notification";

export default new Map([
    ["base", Base],
    ["settings", Settings],
    ["keyValue", KeyValue],
    ["form", Form],
    ["title", Title],

    //notification related actions
    ["notification", Notification]
]);
