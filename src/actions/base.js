export const ActionStates = {
    undefined: "undefined",
    loading: "loading",
    loaded: "loaded",
    succeeded: "succeeded",
    failed: "failed",
    creating: "creating",
    updating: "updating",
    deleting: "deleting"
};

export default class Base {
    static get defaultKey() {
        return undefined;
    }

    constructor(store, settings, key) {
        if (key === undefined) key = this.defaultKey;
        this.currentRequest = 0;
        this.settings = settings;
        this.store = store;
        this.key = key;
    }

    get persistentStore() {
        return localStorage;
    }

    persistentGet() {
        try {
            const value = this.persistentStore.getItem(this.key);
            if (value === null) return {};
            return JSON.parse(value);
        } catch (e) {
            console.warn(e);
            return {};
        }
    }

    persistentSet(value) {
        try {
            this.persistentStore.setItem(this.key, JSON.stringify(value));
            return true;
        } catch (e) {
            console.warn(e);
            return false;
        }
    }

    persistentUpdate(value) {
        const d = this.persistentGet() || {};
        for (const k in value) {
            d[k] = value[k];
        }
        this.persistentSet(d);
    }

    get() {
        return this.store.get(this.key);
    }

    set(value) {
        this.store.set(this.key, value, true);
    }

    update(value) {
        this.store.set(this.key, value);
    }

    handle(promise, onSuccess, onError) {
        promise.then(onSuccess);
        promise.catch(onError);
        return promise;
    }
}

export class Object extends Base {
    constructor(store, settings, key) {
        super(store, settings, key);
        const Api = settings.get(["apis", this.objectType]);
        this.api = new Api(settings, store);
        this.set({
            status: "undefined"
        });
    }
}

export class Details extends Object {
    get(id) {
        this.set({ status: "loading" });
        return this.handle(
            this.api.get(id),
            response => {
                this.set({ data: response.data, status: "loaded" });
            },
            error => {
                this.set({ status: "failed", error: error });
            }
        );
    }
}

export class Create extends Object {
    create(data) {
        this.set({ status: "creating" });
        return this.handle(
            this.api.create(data),
            response => {
                this.set({ status: "succeeded", data: response.data });
            },
            error => {
                this.set({ status: "failed", error: error });
            }
        );
    }
}

export class Update extends Object {
    update(id, data) {
        this.set({ status: "updating" });
        return this.handle(
            this.api.update(id, data),
            response => {
                this.set({ status: "succeeded", data: response.data });
            },
            error => {
                this.set({ status: "failed", error: error });
            }
        );
    }
}

export class Delete extends Object {
    delete(id) {
        this.set({ status: "deleting" });
        return this.handle(
            this.api.delete(id),
            () => {
                this.set({ status: "succeeded" });
            },
            error => {
                this.set({ status: "failed", error: error });
            }
        );
    }
}

export class List extends Object {
    get() {
        this.set({ status: "loading" });
        return this.handle(
            this.api.getList(),
            response => {
                this.set({ data: response.data, status: "loaded" });
            },
            error => {
                this.set({ status: "failed", error: error });
            }
        );
    }
}
