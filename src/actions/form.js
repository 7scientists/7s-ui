import Base from "./base";

export default class Form extends Base {
    setData(key, value) {
        const { data } = this.get();
        data[key] = value;
        const touchedFields = this.touchField(key);
        this.updateForm(data, touchedFields);
    }

    setObjectData(objectKey, key, value) {
        const { data } = this.get();
        if (data[objectKey] === undefined) data[objectKey] = {};
        data[objectKey][key] = value;
        const touchedFields = this.touchField(key);
        this.updateForm(data, touchedFields);
    }

    getProperty(...path) {
        const { data } = this.get();
        let property = data;
        path.forEach(p => {
            property = property[p];
        });
        return property;
    }

    setProperty(key, value, ...path) {
        const { data } = this.get();
        path.reduce((d, e) => {
            if (d[e]) d[e][key] = value;
            else {
                d[e] = {};
                d[e][key] = value;
            }
            return d[e] || {};
        }, data);
        const touchedFields = this.touchField(key);
        this.updateForm(data, touchedFields);
    }

    unsetProperty(key, ...path) {
        const { data } = this.get();
        path.reduce((d, e) => {
            if (d[e]) delete d[e][key];
            return d[e] || {};
        }, data);
        const touchedFields = this.untouchField(key);
        this.updateForm(data, touchedFields);
    }

    createObject(objectKey) {
        const { data } = this.get();
        data[objectKey] = {};
        const touchedFields = this.touchField(objectKey);
        this.updateForm(data, touchedFields);
    }

    renameData(oldKey, newKey) {
        const { data } = this.get();
        const touchedFields = this.replaceTouchedFields(oldKey, newKey);
        data[newKey] = data[oldKey];
        delete data[oldKey];
        this.updateForm(data, touchedFields);
    }

    unsetData(key) {
        const { data } = this.get();
        const touchedFields = this.untouchField(key);
        delete data[key];
        this.updateForm(data, touchedFields);
    }

    unsetObjectData(objectKey, prop) {
        const { data } = this.get();
        const object = data[objectKey];
        delete object[prop];
        const touchedFields = this.untouchField(prop);
        this.updateForm(data, touchedFields);
    }

    touchField(key) {
        const { touchedFields } = this.get();
        if (touchedFields !== undefined && !touchedFields.includes(key))
            touchedFields.push(key);
        return touchedFields;
    }

    replaceTouchedFields(oldKey, newKey) {
        const untouched = this.untouchField(oldKey);
        const touched = this.touchField(newKey);
        return [...new Set(untouched.concat(touched))];
    }

    untouchField(value) {
        const { touchedFields } = this.get();
        const index = touchedFields.indexOf(value);
        if (index !== -1) touchedFields.splice(index, 1);
        return touchedFields;
    }

    reset() {
        this.updateForm(null);
    }

    get Form() {
        throw "not implemented";
    }

    updateForm(data, touchedFields) {
        if (data === null)
            return this.update({
                data: {},
                valid: false,
                error: { errors: {} },
                touchedFields: []
            });
        const form = new this.Form(data, this.settings);

        this.update({
            data: data,
            valid: form.valid,
            error: form.error,
            touchedFields: touchedFields || []
        });
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.reset();
    }
}
