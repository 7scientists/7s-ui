import Base from "./base";

export default class Title extends Base {
    static get defaultKey() {
        return "title";
    }

    setTitle(title) {
        const oldTitle = this.get();
        if (oldTitle === title) return;
        this.set(title);
    }
}
