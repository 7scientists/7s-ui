import Base from "./base";

export default class KeyValue extends Base {
    setData(key, value) {
        const data = this.get();
        data[key] = value;
        this.set(data);
    }

    reset() {
        this.set({});
    }

    constructor(store, settings, key) {
        super(store, settings, key);
        this.reset();
    }
}
