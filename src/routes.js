import NotFound from "7s/components/not_found";

const routes = new Map([
    [
        "notFound",
        {
            handler: () => ({ title: "not-found", component: NotFound })
        }
    ]
]);

export default routes;
