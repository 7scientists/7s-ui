import baseSettings from "./_base";
import genericSettings from "7s/settings/test";
import Settings from "7s/utils/settings";
const settings = new Settings();

settings.update(baseSettings);
settings.update(genericSettings);

export default settings;
