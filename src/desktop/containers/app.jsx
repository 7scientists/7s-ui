import React from "react";
//we import the electron module
import electron from "electron";

export default class App extends React.Component {
    componentDidMount() {
        electron.remote.dialog.showMessageBox({
            message: "It workss! :-)",
            buttons: ["OK"]
        });
    }

    render() {
        return <div>{this.props.children}</div>;
    }
}
