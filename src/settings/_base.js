import Actions from "7s/actions";
import Settings from "7s/utils/settings";

import { convertYAMLTranslations } from "7s/utils/i18n";
import yamlTranslations from "./translations";
import routes from "7s/routes";

const settings = new Settings([
    ["translations", convertYAMLTranslations(yamlTranslations)],
    ["showTitles", true],
    ["routes", routes],
    ["actions", Actions]
]);

export default settings;
