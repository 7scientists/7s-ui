import Settings from "7s/utils/settings";
import baseSettings from "./_base";

const settings = new Settings();
settings.update(baseSettings);
settings.update(new Settings(new Map([["env", "development"]])));

export default settings;
