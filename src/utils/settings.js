String.prototype.format = function() {
    "use strict";
    const str = this.toString();

    const t = typeof arguments[0];
    let args;
    if (arguments.length === 0) args = {};
    else
        args =
            t === "string" || t === "number"
                ? Array.prototype.slice.call(arguments)
                : arguments[0];

    const splits = [];

    let s = str;
    while (s.length > 0) {
        const m = s.match(/\{(?!\{)([\w\d]+)\}(?!\})/);
        if (m !== null) {
            const left = s.substr(0, m.index);
            s = s.substr(m.index + m[0].length);
            const n = parseInt(m[1]);
            splits.push(left);
            // eslint-disable-next-line eqeqeq
            if (n !== n) {
                // not a number
                splits.push(args[m[1]]);
            } else {
                // a numbered argument
                splits.push(args[n]);
            }
        } else {
            splits.push(s);
            s = "";
        }
    }
    return splits;
};

String.prototype.formatStr = function() {
    return this.format(...arguments).join("");
};

export default class Settings {
    constructor() {
        this.map = new Map(...arguments);
    }

    set(key, value) {
        return this.map.set(key, value);
    }

    update(other) {
        this.map = update(this.map, other.map, true, true);
    }

    updateWithMap(otherMap) {
        this.map = update(this.map, otherMap);
    }

    get(key, default_value) {
        let kl = key;
        if (!Array.isArray(kl)) kl = [kl];
        let cv = this.map;
        for (let i = 0; i < kl.length; i++) {
            if (cv === undefined || !(cv instanceof Map)) return default_value;
            cv = cv.get(kl[i]);
        }
        if (cv === undefined) return default_value;
        return cv;
    }

    has(key) {
        return this.map.has(key);
    }

    keys() {
        return this.map.keys();
    }

    lang() {
        return this.get("lang", "en");
    }

    t(key) {
        let kl = key;
        const lang = this.lang();
        if (!Array.isArray(kl)) kl = kl.split(".");
        const value = this.get(["translations", lang, ...kl]);
        if (value === undefined)
            return "[missing translation: {key}/{lang}]".format({
                key: kl.join("/"),
                lang: lang
            });
        if (!(value instanceof String || typeof value === "string"))
            return "[not a string: {key}/{lang}]".format({
                key: kl.join("/"),
                lang: lang
            });
        const params = Array.prototype.slice.call(arguments, 1);
        if (params.length > 0) return value.format(...params);
        return value;
    }
}

export function clone(d) {
    const nd = {};
    Object.keys(d).forEach(key => {
        nd[key] = d[key];
    });
    return nd;
}

export function update(d, ed, overwrite, clone) {
    const assign = (d, key, value) => {
        if (value instanceof Map) {
            const map = new Map([]);
            //we deep-clone the map
            update(map, value, true, false);
            d.set(key, map);
        } else d.set(key, value);
    };

    if (!(ed instanceof Map) || !(d instanceof Map)) {
        throw "Parameters are not maps!";
    }
    if (overwrite === undefined) overwrite = true;
    if (clone === undefined) clone = false;
    if (clone) d = new d.constructor(d);
    for (const key of ed.keys()) {
        const value = ed.get(key);
        const dvalue = d.get(key);
        if (!d.has(key)) {
            assign(d, key, value);
        } else if (value instanceof Map && dvalue instanceof Map) {
            d.set(key, update(dvalue, value, overwrite, clone));
        } else {
            if (!overwrite) continue;
            assign(d, key, value);
        }
    }
    return d;
}
