export function t(translations, key) {
    const str = translations[window.language][key];
    if (str === undefined)
        return "[no translation available for key " + key + "]";
    const formattedStr = str.format(
        ...Array.prototype.slice.call(arguments, 1)
    );
    return formattedStr;
}

export function convertYAMLTranslations(d) {
    const dm = new Map([]);
    for (const key of Object.keys(d)) {
        const value = d[key];
        if (!(typeof key === "string")) continue;
        if (typeof value === "string") {
            dm.set(key, value);
        } else {
            dm.set(key, convertYAMLTranslations(value));
        }
    }
    return dm;
}
