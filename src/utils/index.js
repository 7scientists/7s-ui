export function clone(d) {
    const nd = {};
    Object.keys(d).forEach(key => {
        nd[key] = d[key];
    });
    return nd;
}
