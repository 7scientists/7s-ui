export default class Form {
    constructor(data, settings) {
        this.data = data;
        this.settings = settings;
        this._errors = this.validate();
        this.touchedFields = [];
    }

    get errors() {
        return this._errors;
    }

    get valid() {
        return Object.keys(this._errors).length === 0;
    }

    get errorMessage() {
        return undefined;
    }

    get error() {
        if (this.valid) return { errors: {} };
        return {
            message: this.errorMessage,
            errors: this.errors
        };
    }

    validate() {
        return {};
    }

    //Validation methods

    isKey(key, data) {
        return Object.prototype.hasOwnProperty.call(data, key);
    }

    isKeyDefined(key, data) {
        return this.isKey(key, data) && data[key] !== undefined;
    }

    isEmpty(data) {
        if (data === undefined || data === null || data.length === 0)
            return true;
        return false;
    }

    isPropertyEmpty(data, key) {
        if (
            Object.prototype.hasOwnProperty.call(data, key) &&
            this.isEmpty(data[key])
        )
            return true;
        return false;
    }

    isPropertyNotEmpty(data, key) {
        if (
            Object.prototype.hasOwnProperty.call(data, key) &&
            !this.isEmpty(data[key])
        )
            return true;
        return false;
    }

    isNotEmpty(data) {
        return !this.isEmpty(data);
    }

    isLongerThanOrEqual(x, data) {
        if (data.length >= x) return true;
        return false;
    }

    isShorterThan(x, data) {
        return !this.isLongerThanOrEqual(x, data);
    }

    isValidHostname(data) {
        const hostnameRegEx = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])$/;
        if (this.isNotEmpty(data) && data.match(hostnameRegEx)) return true;
        return false;
    }

    isNotValidHostname(data) {
        return !this.isValidHostname(data);
    }

    isValidIp(data) {
        const ipRegEx = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
        if (this.isNotEmpty(data) && data.match(ipRegEx)) return true;
        return false;
    }

    isNotValidIp(data) {
        return !this.isValidIp(data);
    }

    isValidEndpoint(data) {
        const endpointRegEx =
            "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$";
        if (this.isNotEmpty(data) && data.match(endpointRegEx)) return true;
        return false;
    }

    isNotValidEndpoint(data) {
        return !this.isValidEndpoint(data);
    }

    isNumber(data) {
        return Number.isInteger(Number.parseInt(data));
    }

    isNotNumber(data) {
        return !this.isNumber(data);
    }

    isBoolean(data) {
        return typeof data === "boolean";
    }

    isNotBoolean(data) {
        return !this.isBoolean(data);
    }

    isInRange(data, minThreshold, maxThreshold) {
        return (
            this.isGreaterThanOrEqual(data, minThreshold) &&
            this.isLessThanOrEqual(data, maxThreshold)
        );
    }

    isNotInRange(data, minThreshold, maxThreshold) {
        return !this.isInRange(data, minThreshold, maxThreshold);
    }

    isGreaterThan(data, threshold) {
        return data > threshold;
    }

    isGreaterThanOrEqual(data, threshold) {
        return data >= threshold;
    }

    isLessThan(data, threshold) {
        return data < threshold;
    }

    isLessThanOrEqual(data, threshold) {
        return data <= threshold;
    }
}
