import React from "react";
import PropTypes from "prop-types";

import { A } from ".";

const ButtonIcon = () => (
    <span className="icon is-small">
        <i className="fab fa-github" />
    </span>
);

// ButtonIcon.propTypes = {
//     icon: PropTypes.string.isRequired,
// };

const Button = ({
    primary,
    href,
    outlined,
    icon,
    onClick,
    large,
    disabled,
    children
}) => (
    <div className="has-text-centered github-signup">
        <A
            disabled={disabled}
            className={
                "button" +
                (primary ? " is-primary" : "") +
                (large ? " is-large" : "") +
                (outlined ? " is-outlined" : "")
            }
            onClick={onClick}
            href={href}
        >
            {icon && <ButtonIcon icon={icon} />}
            <span>{children}</span>
        </A>
    </div>
);

Button.defaultProps = {
    disabled: false,
    large: false,
    primary: false,
    outlined: false
};

Button.propTypes = {
    children: PropTypes.node,
    disabled: PropTypes.bool,
    large: PropTypes.bool,
    outlined: PropTypes.bool,
    primary: PropTypes.bool,
    onClick: PropTypes.func.isRequired
};

export default Button;
