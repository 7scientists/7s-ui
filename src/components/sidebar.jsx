import React from "react";
import Menu from "7s/components/menu";
import A from "7s/components/a";
import { withSettings } from "7s/components/settings";

class Sidebar extends React.Component {
    render() {
        const { menu, onToggle, settings, active } = this.props;
        const logo = settings.get("logo");
        return (
            <div>
                <div
                    className={
                        "navbar-burger burger is-hidden-desktop" +
                        (active ? " is-active" : "")
                    }
                    data-target="sidebar"
                    onClick={onToggle}
                >
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <h1 className="logo">
                    <A onClick={onToggle} href="/">
                        <img src={logo} />
                    </A>
                </h1>
                <Menu menu={menu.get("main")} onToggle={onToggle} />
                <Menu
                    menu={menu.get("nav")}
                    onToggle={onToggle}
                    title="Administration"
                />
            </div>
        );
    }
}

export default withSettings(Sidebar);
