import React from "react";
import Modal from "./modal";

export default class Confirm extends React.Component {
    render() {
        const { children } = this.props;
        return <Modal {...this.props}>{children}</Modal>;
    }
}
