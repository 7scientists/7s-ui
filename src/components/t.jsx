import React from "react";
import PropTypes from "prop-types";
import { withSettings } from "./settings";

// eslint-disable-next-line no-unused-vars
const T = ({ k, settings, _original: _, ...args }) => (
    <React.Fragment>{settings.t(k, args)}</React.Fragment>
);

T.propTypes = {
    k: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string)
    ]).isRequired
};

export default withSettings(T);
