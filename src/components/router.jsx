import React from "react";
import PropTypes from "prop-types";

export function withRouter(Component) {
    class Route extends React.Component {
        constructor(props, context) {
            super(props, context);
            //we watch route changes
            const router = this.context.router;
            this.watcherId = router.watch((...args) =>
                this.updateRoute(...args)
            );
            this.state = {
                route: router.currentRoute,
                url: router.currentUrl
            };
        }

        static get isSimple() {
            return Component.isSimple;
        }

        componentWillUnmount() {
            this.context.router.unwatch(this.watcherId);
            this.unmounted = true;
        }

        updateRoute(router, route, url) {
            this.setState({ route: route, url: url });
        }

        render() {
            return (
                <Component
                    {...this.props}
                    _original={this.props}
                    url={this.state.url}
                    route={this.state.route}
                    router={this.context.router}
                />
            );
        }
    }

    Route.contextTypes = {
        router: PropTypes.object
    };

    return Route;
}

class Router extends React.Component {
    getChildContext() {
        return {
            router: this.props.router
        };
    }

    render() {
        return this.props.children;
    }
}

Router.childContextTypes = {
    router: PropTypes.object
};

Router.propTypes = {
    children: PropTypes.node,
    router: PropTypes.object
};

export default Router;
