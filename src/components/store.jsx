import PropTypes from "prop-types";
import React from "react";

import Base from "7s/actions/base";

const actions = {};

export function withActions(Component, actionNameList, keyList, noStore) {
    noStore = !!noStore;

    class Actions extends React.Component {
        static get isSimple() {
            return Component.isSimple;
        }

        componentWillUnmount() {
            if (noStore) return;
            for (const k of this.keyList) {
                this.context.store.unwatch(k, this.watcherIds[k]);
            }
        }

        update(store, key, value) {
            if (key === "") {
                this.setState(value);
            } else {
                const d = {};
                d[key] = value;
                this.setState(d);
            }
        }

        constructor(props, context) {
            super(props, context);

            this.actionProviders = {};
            this.state = {};

            if (!Array.isArray(actionNameList)) {
                actionNameList = [actionNameList];
            }
            if (!Array.isArray(keyList)) {
                keyList = [keyList];
            }

            this.actionNames = actionNameList;
            this.keyList = [];

            for (let i = 0; i < actionNameList.length; i++) {
                const actionName = actionNameList[i];
                let key = keyList[i];

                let ActionProvider;
                let actionProvider;

                if (actionName.prototype instanceof Base) {
                    ActionProvider = actionName;
                } else {
                    ActionProvider = context.settings.get([
                        "actions",
                        actionName
                    ]);
                }

                if (ActionProvider === undefined) {
                    throw "Action provider " + actionName + " is undefined!";
                }

                if (key === undefined) {
                    key = ActionProvider.defaultKey;
                }

                const actionKey = actionName + ":" + key;

                if (actions[actionKey] === undefined) {
                    actionProvider = new ActionProvider(
                        this.context.store,
                        this.context.settings,
                        key
                    );
                    actions[actionKey] = actionProvider;
                }

                actionProvider = actions[actionKey];

                this.actionProviders[key + "Actions"] = actionProvider;
                this.keyList.push(key);
            }

            if (noStore) return;

            const store = this.context.store;
            this.watcherIds = {};
            for (const k of this.keyList) {
                this.watcherIds[k] = store.watch(k, this.update.bind(this));
                //we update the component with the current value of the key
                this.state[k] = store.get(k);
            }
        }

        render() {
            return (
                <Component
                    {...this.props}
                    {...this.state}
                    {...this.actionProviders}
                    store={this.context.store}
                    _original={this.props}
                />
            );
        }
    }

    Actions.contextTypes = {
        store: PropTypes.object,
        settings: PropTypes.object
    };

    return Actions;
}

export function withStore(Component) {
    class Watcher extends React.Component {
        static get isSimple() {
            return Component.isSimple;
        }

        constructor(props, context) {
            super(props, context);
            const { store } = context;
            this.watcherFunction = this.update.bind(this);
            this.watcherId = store.watch("", this.watcherFunction);
        }

        update(store, key) {
            if (key === "") this.forceUpdate();
        }

        componentWillUnmount() {
            this.context.store.unwatch("", this.watcherId);
        }

        render() {
            return (
                <Component
                    {...this.props}
                    {...this.state}
                    store={this.context.store}
                    _original={this.props}
                />
            );
        }
    }

    Watcher.contextTypes = {
        store: PropTypes.object
    };

    return Watcher;
}

export default class Store extends React.Component {
    getChildContext() {
        return {
            store: this.props.store
        };
    }

    render() {
        return this.props.children;
    }
}

Store.childContextTypes = {
    store: PropTypes.object
};
