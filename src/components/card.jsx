import React from "react";

export const Card = ({ children, centered, size, flex, classes }) => (
    <div
        className={
            "ss-card" +
            (centered ? " ss-is-centered" : "") +
            (size !== undefined ? " ss-is-" + size : "") +
            (flex ? " ss-is-flex" : "") +
            (classes !== undefined ? " " + classes : "")
        }
    >
        {children}
    </div>
);

export const CardContent = ({ children, noPadding }) => (
    <div className={"ss-card-content" + (noPadding ? " no-padding" : "")}>
        {children}
    </div>
);

export const CardHeader = ({ children }) => (
    <div className="ss-card-header">{children}</div>
);

export const CardTitle = ({ children }) => (
    <div className="ss-card-title">{children}</div>
);

export const CardFooter = ({ children }) => (
    <div className="ss-card-footer">{children}</div>
);

export const CardIcon = ({ icon }) => (
    <div className="ss-card-icon">
        <i className={"ss-icon fas fa-" + icon} />
    </div>
);
