import React from "react";
import A from "./a";

export class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
    }

    render() {
        const { children } = this.props;
        const { active } = this.state;

        const toggle = () => this.setState({ active: !active });

        return (
            <div
                className={"tabs" + (this.state.active ? " active" : "")}
                onClick={toggle}
            >
                <span className="more">
                    <i className="fas fa-caret-down" />
                </span>
                <ul>{children}</ul>
            </div>
        );
    }
}

export const Tab = ({ children, active, icon, href, params, onClick }) => (
    <li className={active ? "is-active" : ""}>
        <A href={href} params={params} onClick={onClick}>
            <span className="icon is-small">{icon}</span>
            {children}
        </A>
    </li>
);

export const isTab = (tab, name) => {
    return tab !== undefined && tab === name;
};
