import React from "react";

export default function isSimple(Component) {
    class Simple extends React.Component {
        static get isSimple() {
            return true;
        }

        render() {
            return <Component {...this.props} />;
        }
    }
    return Simple;
}
