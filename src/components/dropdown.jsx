import React from "react";
import A from "./a";

export const DropdownMenu = ({ children }) => (
    <Dropdown
        title={
            <span className="icon">
                <i className="fas fa-ellipsis-h" />
            </span>
        }
    >
        <ul className="ss-dropdown-menu">{children}</ul>
    </Dropdown>
);

export const MenuItem = ({ onClick, icon, children }) => (
    <li>
        <A onClick={onClick}>
            <span className="icon">
                <i className={"fas fa-" + icon}></i>
            </span>
            <span>{children}</span>
        </A>
    </li>
);

export class Dropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false,
            right: false
        };
        this.ref = React.createRef();
        this.handler = e => this.handleClick(e);
    }

    hide() {
        this.setState({ expanded: false });
        document.removeEventListener("click", this.handler, false);
    }

    show() {
        this.setState({ expanded: true });
        document.addEventListener("click", this.handler, false);
    }

    handleClick(e) {
        e.preventDefault();
        e.stopPropagation();
        this.hide();
    }

    componentWillUnmount() {
        this.hide();
    }

    componentDidMount() {
        // we check where the dropdown is positioned so that we can
        // display the content either left- or right-aligned
        const rect = this.ref.current.getBoundingClientRect();
        if (rect.left > window.innerWidth * 0.5) {
            this.setState({
                right: true
            });
        }
    }

    render() {
        const { expanded, right } = this.state;
        const { title, children } = this.props;

        const toggle = e => {
            e.preventDefault();
            e.stopPropagation();
            if (!expanded) {
                this.show();
            } else {
                this.hide();
            }
        };

        return (
            <div
                ref={this.ref}
                className={"ss-dropdown" + (right ? " is-right" : "")}
            >
                <a href="" onClick={toggle}>
                    {title}
                </a>
                <div
                    className={
                        "ss-dropdown-content" +
                        (expanded ? " ss-dropdown-expanded" : "")
                    }
                >
                    {children}
                </div>
            </div>
        );
    }
}
