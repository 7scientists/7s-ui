import React from "react";

export const List = ({ children }) => <div className="ss-list">{children}</div>;

export const ListHeader = ({ children }) => (
    <div className="ss-item is-header">{children}</div>
);

export const ListColumn = ({ children, size = "md" }) => (
    <div className={"ss-col is-" + size}>{children}</div>
);

export const ListItem = ({ children, isCard = true, onClick }) => (
    <div className={"ss-item" + (isCard ? " is-card" : "")} onClick={onClick}>
        {children}
    </div>
);
