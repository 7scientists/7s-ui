import React from "react";
import PropTypes from "prop-types";

/** A heading of second order. */
export default class Title extends React.Component {
    render() {
        return <h2 className="title">{this.props.title}</h2>;
    }
}

Title.propTypes = {
    title: PropTypes.node.isRequired
};
