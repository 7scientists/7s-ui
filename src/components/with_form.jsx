import React from "react";
import { withActions } from "./store";
import { withSettings } from "./settings";

export default function withForm(Component, Form, key) {
    class WithForm extends React.Component {
        constructor() {
            super();
        }

        static get isSimple() {
            return Component.isSimple;
        }

        componentDidMount() {
            const form = this.getForm();
            this.manageChange(form);
        }

        manageChange(form) {
            const { onChange, onError } = this.props;

            if (form.valid && onChange !== undefined) onChange(form.data);
            // we report form changes
            else if (!form.valid && onError !== undefined) onError(form.data); // we report errors
        }

        getForm(data) {
            const { settings } = this.props;
            if (data === undefined) data = this.props[key] || {};
            return new Form(data, settings);
        }

        render() {
            const actions = this.props[key + "Actions"];
            const form = this.getForm();

            const set = (k, v) => {
                actions.setData(k, v);
                const data = actions.get();
                const form = this.getForm(data);
                this.manageChange(form);
            };

            const d = {};

            d[key] = {
                set: set,
                data: form.data,
                error: form.error,
                valid: form.valid
            };

            return <Component {...this.props} {...d} />;
        }
    }

    return withSettings(withActions(WithForm, ["keyValue"], [key]));
}
