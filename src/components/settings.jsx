import PropTypes from "prop-types";
import React from "react";
import { withActions } from "./store";

export function withSettings(Component) {
    class Settings extends React.Component {
        static get isSimple() {
            return Component.isSimple;
        }

        render() {
            return (
                <Component
                    {...this.props}
                    _original={this.props}
                    settings={this.context.settings}
                />
            );
        }
    }

    Settings.contextTypes = {
        settings: PropTypes.object
    };

    return Settings;
}

export default class Settings extends React.Component {
    getChildContext() {
        return {
            settings: this.props.settings
        };
    }

    render() {
        return this.props.children;
    }
}

Settings.childContextTypes = {
    settings: PropTypes.object
};

class ExtSettings extends React.Component {
    constructor(props, context) {
        super(props, context);
        props.settingsActions.load();
    }

    render() {
        const { settings } = this.props;
        if (settings.status !== "loaded") return <div />;
        return this.props.children;
    }
}

export const ExternalSettings = withActions(ExtSettings, ["settings"]);
