import React from "react";
import PropTypes from "prop-types";

//import './badge.scss';

const Badge = ({ shape, classes, children, onClick }) => {
    const shapeClass = shape === "rect" ? "rect" : "round";

    return (
        <span
            onClick={onClick}
            className={"badge " + shapeClass + (classes ? " " + classes : "")}
        >
            {children}
        </span>
    );
};

Badge.propTypes = {
    children: PropTypes.node.isRequired,
    classes: PropTypes.string,
    shape: PropTypes.oneOf(["rect", "round"]),
    onClick: PropTypes.func.isRequired
};

export default Badge;
