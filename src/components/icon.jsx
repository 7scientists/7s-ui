import React from "react";

const Icon = ({ icon, iconClasses }) => (
    <i className={"fas fa-" + icon + (iconClasses ? " " + iconClasses : "")} />
);

export const Icon2 = ({ icon, iconClasses, containerClasses }) => (
    <span className={"icon" + " " + containerClasses}>
        <Icon icon={icon} iconClasses={iconClasses} />
    </span>
);

export default Icon;
