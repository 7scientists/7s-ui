import React from "react";

export const mergeErrors = (errors, newErrors) => {
    for (const key of Object.keys(newErrors)) {
        if (errors[key] === undefined) errors[key] = newErrors[key];
        else errors[key] = errors[key].concat(newErrors[key]);
    }
};

export const ErrorMessage = props => {
    const { error } = props;
    if (error === undefined || error.message === undefined) return null;
    return (
        <div className="message is-danger">
            <div className="message-body">{error.message}</div>
        </div>
    );
};

export const ErrorFor = props => {
    const { error, field } = props;
    let message;
    if (
        error === undefined ||
        error.errors === undefined ||
        !error.errors[field]
    )
        message = null;
    else message = error.errors[field];
    return <p className="help is-danger">{message}</p>;
};

export const ErrorForWithTouched = props => {
    const { error, field, touchedFields } = props;
    let message;
    if (
        error === undefined ||
        error.errors === undefined ||
        !error.errors[field]
    )
        message = null;
    else message = error.errors[field];
    if (touchedFields !== undefined && touchedFields.includes(field))
        return <p className="help is-danger">{message}</p>;
    return null;
};
