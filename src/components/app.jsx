import React from "react";
import Nav from "7s/components/nav";
import Sidebar from "7s/components/sidebar";
import WithSidebar from "7s/components/with_sidebar";
import { withActions } from "7s/components/store";
import { withRouter } from "7s/components/router";
import { withSettings } from "7s/components/settings";
import Notification from "7s/components/notification";

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.updateTitle();

        this.state = {
            active: false
        };
    }

    onToggle() {
        this.setState({ active: !this.state.active });
    }

    componentDidUpdate(prevProps) {
        const { component } = this.props.route;
        const oldComponent = prevProps.route.component;
        //we only update the title if the component changed
        if (component !== oldComponent) this.updateTitle();
    }

    updateTitle() {
        const { props } = this;
        const { route, settings } = props;
        const newTitle =
            settings.t(["nav", route.title, "title"]) ||
            settings.t(["nav", "no-title"]);
        props.titleActions.setTitle(newTitle);
    }

    render() {
        const { route } = this.props;
        const RouteComponent = route.component;

        if (RouteComponent.isSimple)
            return this.renderSimple(RouteComponent, route.props);

        return this.renderFull(RouteComponent, route.props);
    }

    renderFull(Component, props) {
        const { menu } = this.props;
        const { active } = this.state;

        const content = (
            <div>
                <Nav
                    active={active}
                    menu={menu.get("nav")}
                    onToggle={() => {
                        this.onToggle();
                    }}
                />
                <div className="main">
                    <div className="container is-fluid">
                        <Component {...props} />
                    </div>
                </div>
            </div>
        );
        const sidebar = (
            <Sidebar
                menu={menu}
                active={active}
                onToggle={() => {
                    this.onToggle();
                }}
            />
        );
        return (
            <div>
                <Notification />
                <WithSidebar
                    active={active}
                    sidebar={sidebar}
                    content={content}
                />
            </div>
        );
    }

    renderSimple(Component, props) {
        return <Component {...props} />;
    }
}

export default withSettings(withRouter(withActions(App, ["title"])));
