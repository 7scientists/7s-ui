import React from "react";
import { Card, CardContent } from ".";

/* eslint-disable no-unused-vars */

const CenteredCard = ({ title, footer, children }) => {
    let titleDiv, footerDiv;
    if (title !== undefined)
        titleDiv = (
            <div className="card-header">
                <p className="card-header-title">{title}</p>
            </div>
        );
    if (footer !== undefined) {
        footerDiv = (
            <div className="card-footer">
                <p className="card-footer-item">{footer}</p>
            </div>
        );
    }
    return (
        <section className="simple is-info is-fullheight">
            <Card centered>
                <CardContent centered>{children}</CardContent>
            </Card>
        </section>
    );
};

export default CenteredCard;
