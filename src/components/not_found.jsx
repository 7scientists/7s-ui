import React from "react";
import A from "./a";
import { Icon2 as Icon } from "./icon";

export default class NotFound extends React.Component {
    render() {
        return <p>The URL you were looking for does not exist.</p>;
    }
}

export const NotFoundDetails = ({
    title,
    text,
    hasButton = false,
    buttonIcon,
    buttonText,
    buttonHref
}) => (
    <div>
        <h3>{title}</h3>
        <p>{text}</p>
        {hasButton && (
            <A className="button is-large" href={buttonHref}>
                {buttonIcon && <Icon icon={buttonIcon} iconClasses="fa-fw" />}
                <span>{buttonText}</span>
            </A>
        )}
    </div>
);
