import React from "react";
import PropTypes from "prop-types";

export default class Modal extends React.Component {
    render() {
        const {
            children,
            title,
            save,
            saveDisabled,
            cancelDisabled,
            cancel,
            onSave,
            onCancel,
            onClose,
            saveClass
        } = this.props;
        return (
            <div className="modal is-active">
                <div className="modal-background" onClick={onClose}></div>
                <div className="modal-card">
                    <header className="modal-card-head">
                        <p className="modal-card-title">{title}</p>
                        {onClose && (
                            <button
                                type="button"
                                className="delete"
                                aria-label="Close modal"
                                onClick={onClose}
                            ></button>
                        )}
                    </header>
                    <section className="modal-card-body">{children}</section>
                    <footer className="modal-card-foot">
                        {save && (
                            <button
                                type="submit"
                                className={"button " + saveClass}
                                disabled={saveDisabled}
                                onClick={onSave}
                            >
                                {save}
                            </button>
                        )}
                        {cancel && (
                            <button
                                type="button"
                                className="button"
                                disabled={cancelDisabled}
                                onClick={onCancel}
                            >
                                {cancel}
                            </button>
                        )}
                    </footer>
                </div>
            </div>
        );
    }
}

Modal.defaultProps = {
    cancel: undefined,
    cancelDisabled: false,
    saveDisabled: false,
    saveClass: "is-success"
};

Modal.propTypes = {
    cancel: PropTypes.string,
    cancelDisabled: PropTypes.bool,
    children: PropTypes.node,
    save: PropTypes.bool.isRequired,
    saveClass: PropTypes.string,
    saveDisabled: PropTypes.bool,
    title: PropTypes.string,
    onCancel: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired
};
