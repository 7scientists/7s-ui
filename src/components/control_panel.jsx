import React from "react";
import Badge from "7s/components/badge";
import { Icon2 as Icon } from "7s/components/icon";
import { A } from "7s/components";

const ControlPanelHeader = ({ children }) => (
    <div className="control-panel-header">{children}</div>
);

const ControlPanelBody = ({ children }) => (
    <div className="control-panel-body">{children}</div>
);

const ControlPanelFooter = ({ children }) => (
    <div className="control-panel-footer">{children}</div>
);

export const ControlPanelList = ({ children }) => <ul>{children}</ul>;

export const ControlPanelItem = ({
    name,
    onClick,
    href,
    actionIcon,
    onActionClick,
    isSelected,
    children
}) => {
    const actionBadge = (
        <Badge classes="is-dark" onClick={onActionClick}>
            <Icon
                icon={actionIcon}
                iconClasses="fa-fw fa-inverse"
                containerClasses="is-small"
            />
        </Badge>
    );

    let content = (
        <div>
            <span>{name}</span> {actionBadge}
        </div>
    );
    if (href)
        content = (
            <A href={href}>
                <span>{name}</span> {actionBadge}
            </A>
        );

    return (
        <li className={isSelected ? "selected" : null} onClick={onClick}>
            {content}
            {children}
        </li>
    );
};

export default class ControlPanel extends React.Component {
    render() {
        const { notFoundText, headerText, items, footer } = this.props;

        const itemList = items ? <ul>{items}</ul> : notFoundText;

        return (
            <div className="control-panel">
                <ControlPanelHeader>
                    <h3>{headerText}</h3>
                </ControlPanelHeader>
                <ControlPanelBody>{itemList}</ControlPanelBody>
                <ControlPanelFooter>{footer}</ControlPanelFooter>
            </div>
        );
    }
}
