import React from "react";

export const UP = "up";
export const DOWN = "down";

export default class Direction extends React.Component {
    constructor(props) {
        super(props);
        const { defaultDirection } = props;

        this.state = { direction: DOWN };
        if (
            defaultDirection !== undefined &&
            (defaultDirection === UP || defaultDirection === DOWN)
        )
            this.state = { direction: defaultDirection };
    }

    arrow() {
        const { direction } = this.state;
        const { iconUp, iconDown } = this.props;

        switch (direction) {
            case UP:
                return (
                    <i
                        className={"fas fa-" + (iconUp || "arrow-up")}
                        aria-hidden="true"
                    ></i>
                );
            case DOWN:
                return (
                    <i
                        className={"fas fa-" + (iconDown || "arrow-down")}
                        aria-hidden="true"
                    ></i>
                );
        }
    }

    toggle() {
        const { changeDirection } = this.props;
        const { direction } = this.state;

        if (changeDirection !== undefined) changeDirection();

        switch (direction) {
            case UP:
                this.setState({ direction: DOWN });
                break;
            case DOWN:
                this.setState({ direction: UP });
                break;
        }
    }

    render() {
        const arrow = this.arrow();
        return (
            <button className="button" onClick={this.toggle.bind(this)}>
                {arrow}
            </button>
        );
    }
}
