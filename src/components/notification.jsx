import React from "react";
import { withActions } from "7s/components/store";
import {
    PRIMARY,
    INFO,
    SUCCESS,
    WARNING,
    DANGER
} from "7s/actions/notification";

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.ref = React.createRef();
    }

    componentDidMount() {
        const { id, timeout, clearNotification } = this.props;

        this.timeout = setTimeout(() => {
            this.ref.current.classList.add("is-hidden");
            clearNotification(id);
        }, timeout);
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    hide() {
        this.ref.current.classList.add("is-hidden");
    }

    getTypeClass(type) {
        switch (type) {
            case PRIMARY:
                return "is-primary";
            case INFO:
                return "is-info";
            case SUCCESS:
                return "is-success";
            case WARNING:
                return "is-warning";
            case DANGER:
                return "is-danger";
            default:
                return null;
        }
    }

    render() {
        const { title, text, type } = this.props;

        const typeClass = this.getTypeClass(type);

        return (
            <div
                ref={this.ref}
                className={"notification" + (typeClass ? " " + typeClass : "")}
            >
                <button className="delete" onClick={() => this.hide()}></button>
                <p>
                    <strong>{title}</strong>
                </p>
                <p>{text}</p>
            </div>
        );
    }
}

class NotificationContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { notification, notificationActions } = this.props;

        const notificationsList = notification.list
            .reverse()
            .map(notification => {
                return (
                    <Notification
                        key={notification.id}
                        id={notification.id}
                        title={notification.title}
                        text={notification.text}
                        timeout={notification.timeout}
                        type={notification.type}
                        clearNotification={id =>
                            notificationActions.clearNotification(id)
                        }
                    />
                );
            });

        return (
            <div className="notification-container">{notificationsList}</div>
        );
    }
}

export default withActions(NotificationContainer, ["notification"]);
