import React from "react";
import PropTypes from "prop-types";

class WithHeader extends React.Component {
    render() {
        return (
            <div className="with-header">
                {this.props.header && (
                    <section className="header">{this.props.header}</section>
                )}
                {this.props.content}
            </div>
        );
    }
}

WithHeader.propTypes = {
    content: PropTypes.node,
    header: PropTypes.node
};

export default WithHeader;
