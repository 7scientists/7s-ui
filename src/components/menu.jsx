import React from "react";
import A from "7s/components/a";
import { withStore } from "7s/components/store";

class NavItem extends React.Component {
    render() {
        const { href, onToggle } = this.props;
        return (
            <li>
                <A href={href} onClick={onToggle}>
                    {this.props.children}
                </A>
            </li>
        );
    }
}

class DropdownItem extends React.Component {
    render() {
        const { href, onToggle } = this.props;
        return (
            <li>
                <A href={href} onClick={onToggle}>
                    {this.props.children}
                </A>
            </li>
        );
    }
}

class DropdownMenu extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            active: false
        };
        this.self = React.createRef();
        this.handler = e => {
            const { active } = this.state;
            if (!active) return;
            const item = this.self;
            //if the click was inside the active submenu, we process it normally
            if (item !== undefined && item.contains(e.target)) return;
            this.setState({ active: false });
        };
    }

    isActive() {
        const { active } = this.state;
        return active;
    }

    onToggle(e) {
        this.toggleActive();
        this.props.onToggle(e);
    }

    toggleActive() {
        this.setState({ active: !this.state.active });
    }

    componentDidMount() {
        document.addEventListener("click", this.handler);
    }

    componentWillUnmount() {
        document.removeEventListener("click", this.handler);
    }

    render() {
        const { title, items, store } = this.props;
        const navItems = items
            .filter(item => {
                if (item.show !== undefined && item.show(store) === false)
                    return false;
                return true;
            })
            .map((item, i) => {
                let href;
                if (item.route !== undefined) href = "/" + item.route;
                return (
                    <DropdownItem
                        key={i}
                        href={href}
                        onToggle={e => {
                            this.onToggle(e);
                        }}
                    >
                        {item.title}
                    </DropdownItem>
                );
            });
        let subMenu;
        if (this.isActive()) {
            subMenu = <ul>{navItems}</ul>;
        }
        return (
            <li ref={this.self}>
                <A
                    className={this.isActive() ? "is-active" : ""}
                    onClick={e => {
                        this.toggleActive();
                        e.preventDefault();
                    }}
                >
                    {title}
                </A>
                {subMenu}
            </li>
        );
    }
}

class MenuItems extends React.Component {
    render() {
        const { menu, onToggle, store } = this.props;
        const items = [];

        for (const [, item] of menu) {
            items.push(item);
        }

        return items
            .filter(item => {
                if (item.show !== undefined && item.show(store) === false)
                    return false;
                return true;
            })
            .map((item, i) => {
                if (item.subMenu !== undefined) {
                    return (
                        <DropdownMenu
                            key={i}
                            title={item.title}
                            items={item.subMenu}
                            onToggle={onToggle}
                        />
                    );
                } else {
                    let href;
                    if (item.route !== undefined) href = "/" + item.route;
                    return (
                        <NavItem key={i} href={href} onToggle={onToggle}>
                            {item.title}
                        </NavItem>
                    );
                }
            });
    }
}

class Menu extends React.Component {
    render() {
        const { title, menu, onToggle, store } = this.props;
        let titleContent;
        if (title !== undefined)
            titleContent = <p className="menu-label">{title}</p>;

        return (
            <aside className="menu">
                {titleContent}
                <ul className="menu-list">
                    <MenuItems menu={menu} onToggle={onToggle} store={store} />
                </ul>
            </aside>
        );
    }
}

export default withStore(Menu);
