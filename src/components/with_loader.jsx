import React from "react";

const renderWait = () => {
    return (
        <article className="message is-info">
            <div className="message-body">
                <p>Please wait, we are loading the requested resource...</p>
            </div>
        </article>
    );
};

const renderFailed = () => {
    return (
        <article className="message is-danger">
            <div className="message-body">
                <p>
                    We are sorry but we could not load the requested resource.
                </p>
            </div>
        </article>
    );
};

const renderLoaded = () => {
    return <div />;
};

function allAre(props, status) {
    for (const resource of props.resources) {
        if (resource === undefined || resource.status !== status) {
            return false;
        }
    }
    return true;
}

function oneIs(props, status) {
    for (const resource of props.resources) {
        if (resource !== undefined && resource.status === status) return true;
    }
    return false;
}

function isFailed(props) {
    return oneIs(props, "failed");
}

function isLoaded(props) {
    return allAre(props, "loaded");
}

class WithLoader extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.update(props);
    }

    componentDidUpdate() {
        this.update(this.props);
    }

    update(props) {
        if (props.onLoad !== undefined && isLoaded(props)) props.onLoad();
    }

    render() {
        const { resources } = this.props;
        if (isLoaded(this.props)) return this.props.renderLoaded();
        else if (isFailed(this.props))
            return this.props.renderFailed(resources);
        return this.props.renderWait();
    }
}

WithLoader.defaultProps = {
    renderWait: renderWait,
    renderFailed: renderFailed,
    renderLoaded: renderLoaded
};

export default WithLoader;
