import React from "react";
import A from "./a";
import { Control } from "./form";
import { getDisplayName } from "../helpers/hoc";
import Direction, { UP, DOWN } from "./direction";
import { ASC, DESC } from "../actions/sort";

export class DropdownTrigger extends React.Component {
    render() {
        const { selectionText, toggleMenu } = this.props;

        let text = "Select...";
        if (selectionText !== undefined) text = selectionText;

        return (
            <div className="dropdown-trigger">
                <button
                    className="button"
                    onClick={toggleMenu}
                    aria-haspopup="true"
                    aria-controls="dropdown-menu"
                >
                    <span>{"Sort by: " + text}</span>
                    <span className="icon is-small">
                        <i className="fas fa-angle-down" aria-hidden="true"></i>
                    </span>
                </button>
            </div>
        );
    }
}

export class DropdownItem extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = { isActive: false };
    }

    render() {
        const { text, isActive, onClick } = this.props;

        return (
            <A
                href=""
                className={"dropdown-item" + (isActive ? " is-active" : "")}
                onClick={onClick}
            >
                {text}
            </A>
        );
    }
}

export class Dropdown extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isActive: false,
            isRight: false
        };

        this.ref = React.createRef();
    }

    componentDidMount() {
        const rect = this.ref.current.getBoundingClientRect();
        if (rect.left > window.innerWidth * 0.5) {
            this.setState({ isRight: true });
        }
    }

    componentWillUnmount() {
        this.setState({ isActive: false });
    }

    toggle() {
        this.setState({ isActive: !this.state.isActive });
    }

    render() {
        const { children, selectionText } = this.props;

        const classes = [
            "dropdown",
            this.state.isActive ? " is-active" : "",
            this.state.isRight ? " is-right" : ""
        ];

        return (
            <div>
                <div
                    ref={this.ref}
                    className={classes.filter(Boolean).join(" ")}
                    onClick={this.toggle.bind(this)}
                >
                    <DropdownTrigger selectionText={selectionText} />
                    <div
                        className="dropdown-menu"
                        id="dropdown-menu"
                        role="menu"
                    >
                        <div className="dropdown-content">{children}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export class SortDropdown extends React.Component {
    constructor(props, context) {
        super(props, context);
        const defaultItem = props.defaultItem || {
            field: undefined,
            text: undefined
        };
        this.state = {
            defaultItem: defaultItem
        };
    }

    select(item, onChange) {
        onChange(item); //update store
        this.setState({ defaultItem: item });
    }

    render() {
        const { items, onChange } = this.props;
        const { defaultItem } = this.state;

        const dropdownItems = items.map(item => {
            let isActive = false;
            if (defaultItem.field === item.field) isActive = !isActive;
            return (
                <DropdownItem
                    key={item.field}
                    text={item.text}
                    isActive={isActive}
                    onClick={() => this.select(item, onChange)}
                />
            );
        });

        return (
            <Dropdown selectionText={defaultItem.text}>
                {dropdownItems}
            </Dropdown>
        );
    }
}

export function withSortAction(Component, actionName) {
    class Sort extends React.Component {
        constructor(props, context) {
            super(props, context);

            this.actionName = actionName + "Actions";

            const {
                [this.actionName]: sortActions,
                defaultItem,
                defaultOrder
            } = props;

            if (defaultItem !== undefined)
                sortActions.updateSortField(defaultItem);

            this.state = { sortOrder: DOWN };
            if (
                defaultOrder !== undefined &&
                (defaultOrder === UP || defaultOrder === DOWN)
            ) {
                this.state = { sortOrder: defaultOrder };

                switch (defaultOrder) {
                    case UP:
                        sortActions.updateSortOrder(ASC);
                        break;
                    case DOWN:
                        sortActions.updateSortOrder(DESC);
                        break;
                }
            }
        }

        resetSort() {
            throw "not implemented yet";
        }

        onChangeSortField(item) {
            const { [this.actionName]: sortActions } = this.props;
            sortActions.updateSortField(item);
        }

        onChangeSortOrder() {
            const { [this.actionName]: sortActions } = this.props;
            const { sortOrder } = this.state;

            switch (sortOrder) {
                case UP:
                    sortActions.updateSortOrder(DESC);
                    this.setState({ sortOrder: DOWN });
                    break;
                case DOWN:
                    sortActions.updateSortOrder(ASC);
                    this.setState({ sortOrder: UP });
                    break;
            }
        }

        render() {
            return (
                <Component
                    onChangeSortField={item => this.onChangeSortField(item)}
                    onChangeSortOrder={() => this.onChangeSortOrder()}
                    {...this.props}
                />
            );
        }
    }

    Sort.displayName = `WithSortAction(${getDisplayName(Component)})`;

    return Sort;
}

export default class BaseSort extends React.Component {
    render() {
        const {
            items,
            defaultItem,
            defaultOrder,
            onChangeSortField,
            onChangeSortOrder
        } = this.props;

        return (
            <div className="field has-addons">
                <Control>
                    <SortDropdown
                        items={items}
                        defaultItem={defaultItem}
                        onChange={item => onChangeSortField(item)}
                    />
                </Control>
                <Control>
                    <Direction
                        iconUp="sort-alpha-down"
                        iconDown="sort-alpha-up"
                        defaultDirection={defaultOrder}
                        changeDirection={() => onChangeSortOrder()}
                    />
                </Control>
            </div>
        );
    }
}
