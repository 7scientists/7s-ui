import React from "react";

const Message = ({ type, children }) => (
    <div className={"message is-" + type}>
        <div className="message-body">{children}</div>
    </div>
);

export default Message;
