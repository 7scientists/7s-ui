import React from "react";
import PropTypes from "prop-types";
import Title from "./_title";
import { withSettings } from "./settings";

/** Puts a title above its content. */
class Titled extends React.Component {
    render() {
        const { children, settings, title } = this.props;
        if (!settings.get("showTitles")) {
            return children;
        }
        return (
            <div id="wrapper">
                <Title title={title} />
                {children}
            </div>
        );
    }
}

Titled.propTypes = {
    /** The content to display */
    children: PropTypes.node,
    settings: PropTypes.shape({
        get: PropTypes.func
    }),
    /** The title to display above the content */
    title: PropTypes.node
};

export default withSettings(Titled);
