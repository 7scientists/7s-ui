import React from "react";

export default class WithSidebar extends React.Component {
    render() {
        const { active } = this.props;
        return (
            <div className="with-sidebar">
                <div className={"sidebar" + (active ? " is-active" : "")}>
                    {this.props.sidebar}
                </div>
                <div className="sidebar-content">{this.props.content}</div>
            </div>
        );
    }
}
