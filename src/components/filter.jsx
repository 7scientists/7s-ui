import React from "react";
import { Form, Field, Input, Control } from "./form";
import { getDisplayName } from "../helpers/hoc";

export function withFilterAction(Component, actionName) {
    class Filter extends React.Component {
        constructor(props, context) {
            super(props, context);
            this.actionName = actionName + "Actions";
        }

        clearFilter() {
            const { [this.actionName]: filterActions } = this.props;
            filterActions.clearFilterTerm();
        }

        onChange(value) {
            const { [this.actionName]: filterActions } = this.props;
            filterActions.updateFilterTerm(value);
        }

        render() {
            return (
                <Component
                    onChange={value => this.onChange(value)}
                    {...this.props}
                />
            );
        }
    }

    Filter.displayName = `WithFilterAction(${getDisplayName(Component)})`;

    return Filter;
}

export default class BaseFilter extends React.Component {
    render() {
        const { placeHolder, onChange } = this.props;
        return (
            <Form>
                <Field>
                    <Control>
                        <Input
                            type="text"
                            placeHolder={placeHolder}
                            onChange={value => onChange(value)}
                        />
                    </Control>
                </Field>
            </Form>
        );
    }
}
