import React from "react";
import PropTypes from "prop-types";
import { clone } from "7s/utils";
import { withRouter } from "./router";

class A extends React.Component {
    get href() {
        const { href, params } = this.props;
        return params ? `${href}?${this._encode(params)}` : href;
    }

    onClick(e) {
        if (this.props._original.onClick !== undefined)
            this.props._original.onClick(e);
        if (e.defaultPrevented) return;
        e.preventDefault();
        this.props.router.navigateToUrl(this.href);
    }

    _encode(params) {
        return Object.keys(params)
            .map(function(k) {
                return (
                    encodeURIComponent(k) + "=" + encodeURIComponent(params[k])
                );
            })
            .join("&");
    }

    render() {
        const props = clone(this.props._original);
        props.onClick = this.onClick.bind(this);
        props.href = this.href;
        return <a {...props}>{this.props.children}</a>;
    }
}

A.propTypes = {
    _original: PropTypes.shape({
        onClick: PropTypes.func
    }).isRequired,
    children: PropTypes.node.isRequired,
    href: PropTypes.string,
    params: PropTypes.object
};

export { A };

export default withRouter(A);
