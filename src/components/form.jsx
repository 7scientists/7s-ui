import React from "react";
import PropTypes from "prop-types";

export const Form = ({ onSubmit, children, id }) => (
    <form id={id} onSubmit={onSubmit}>
        {children}
    </form>
);
Form.propTypes = {
    children: PropTypes.node,
    id: PropTypes.string,
    onSubmit: PropTypes.func.isRequired
};

export const Field = ({ children }) => <div className="field">{children}</div>;
Field.propTypes = {
    children: PropTypes.node
};

export const Label = ({ children, htmlFor, className = "label" }) => (
    <label htmlFor={htmlFor} className={className}>
        {children}
    </label>
);
Label.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    htmlFor: PropTypes.string
};

export const SubmitButton = ({ disabled, children }) => (
    <button disabled={disabled}>{children}</button>
);
SubmitButton.propTypes = {
    children: PropTypes.node,
    disabled: PropTypes.bool
};

export const Control = ({ children, className }) => (
    <div className={"control" + (className ? " " + className : "")}>
        {children}
    </div>
);
Control.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string
};

export const Select = ({
    className,
    items,
    defaultValue,
    disabled = false,
    onChange
}) => {
    const options = items.map(item => (
        <option key={item.value} value={item.value}>
            {item.text}
        </option>
    ));
    return (
        <div className={"select" + (className ? " " + className : "")}>
            <select
                defaultValue={defaultValue}
                disabled={disabled}
                onChange={e => onChange(e.target.value)}
            >
                {options}
            </select>
        </div>
    );
};
Select.propTypes = {
    className: PropTypes.string,
    defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    disabled: PropTypes.bool,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        })
    ).isRequired,
    onChange: PropTypes.func.isRequired
};

export const Checkbox = ({ value, name, onChange, defaultChecked }) => (
    <input
        type="checkbox"
        onChange={e => onChange(e.target.checked)}
        name={name}
        id={name}
        value={value}
        defaultChecked={defaultChecked}
    />
);
Checkbox.propTypes = {
    defaultChecked: PropTypes.bool,
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

export const Input = ({
    value,
    type,
    name,
    onChange,
    className = "input",
    id,
    placeHolder,
    autoComplete
}) => (
    <input
        id={id}
        className={className}
        type={type}
        onChange={e => onChange(e.target.value)}
        name={name}
        value={value}
        placeholder={placeHolder}
        autoComplete={autoComplete}
    />
);
Input.propTypes = {
    autoComplete: PropTypes.string,
    className: PropTypes.bool,
    id: PropTypes.string,
    name: PropTypes.string,
    placeHolder: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

export const FieldSet = ({ children, disabled = false }) => (
    <fieldset disabled={disabled}>{children}</fieldset>
);
FieldSet.propTypes = {
    children: PropTypes.node,
    disabled: PropTypes.bool
};
