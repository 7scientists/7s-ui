import React from "react";
import { withStore } from "7s/components/store";
import A from "7s/components/a";

class NavItem extends React.Component {
    render() {
        const { href, onToggle } = this.props;
        return (
            <A className="navbar-link" href={href} onClick={onToggle}>
                {this.props.children}
            </A>
        );
    }
}

class DropdownItem extends React.Component {
    render() {
        const { href, onToggle } = this.props;
        return (
            <A className="dropdown-item" href={href} onClick={onToggle}>
                {this.props.children}
            </A>
        );
    }
}

class DropdownMenu extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            active: false
        };
        this.self = React.createRef();
        this.handler = e => {
            const { active } = this.state;
            if (!active) return;
            const item = this.self;
            //if the click was inside the active submenu, we process it normally
            if (item !== undefined && item.contains(e.target)) return;
            this.setState({ active: false });
        };
    }

    isActive() {
        return this.state.active;
    }

    onToggle(e) {
        this.toggleActive();
        this.props.onToggle(e);
    }

    toggleActive() {
        this.setState({ active: !this.state.active });
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handler);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handler);
    }

    render() {
        const { title, items } = this.props;
        const navItems = items.map((item, i) => {
            let href;
            if (item.route !== undefined) href = "/" + item.route;
            return (
                <DropdownItem
                    key={i}
                    href={href}
                    onToggle={e => {
                        this.onToggle(e);
                    }}
                >
                    {item.title}
                </DropdownItem>
            );
        });
        return (
            <div
                ref={this.self}
                className={
                    "navbar-item has-dropdown" +
                    (this.isActive() ? " is-active" : "")
                }
            >
                <A
                    className="navbar-link"
                    onClick={e => {
                        this.toggleActive();
                        e.preventDefault();
                    }}
                >
                    {title}
                </A>
                <div className="navbar-dropdown is-right">{navItems}</div>
            </div>
        );
    }
}

class MenuItems extends React.Component {
    render() {
        const { menu, onToggle, store } = this.props;
        const items = [];
        for (const [, item] of menu) {
            items.push(item);
        }
        return items
            .filter(item => {
                if (item.show !== undefined && item.show(store) === false)
                    return false;
                return true;
            })
            .map((item, i) => {
                if (item.subMenu !== undefined) {
                    return (
                        <DropdownMenu
                            key={i}
                            title={item.title}
                            items={item.subMenu}
                            onToggle={onToggle}
                        />
                    );
                } else {
                    let href;
                    if (item.route !== undefined) href = "/" + item.route;
                    return (
                        <NavItem key={i} href={href} onToggle={onToggle}>
                            {item.title}
                        </NavItem>
                    );
                }
            });
    }
}

class Nav extends React.Component {
    render() {
        const { menu, active, onToggle, store } = this.props;
        const title = store.get("title");

        return (
            <nav
                className="navbar"
                role="navigation"
                aria-label="main navigation"
            >
                <div className="container is-fluid">
                    <div className="navbar-brand">
                        <A className="navbar-item navbar-title" href="/">
                            {title}
                        </A>

                        <div
                            className={
                                "navbar-burger burger is-hidden-desktop" +
                                (active ? " is-active" : "")
                            }
                            data-target="sidebar"
                            onClick={onToggle}
                        >
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div id="menu" key="menu" className={"navbar-menu"}>
                        <div className="navbar-end" key="menubar">
                            <MenuItems
                                menu={menu}
                                onToggle={onToggle}
                                store={store}
                            />
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

// nav watches the entire store as the menu items depend on it...
export default withStore(Nav);
