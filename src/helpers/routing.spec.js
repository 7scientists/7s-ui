import { Router } from "./routing.js";

describe("Router", () => {
    describe("parseParameters", () => {
        test("returns an empty object when there are no parameters", () => {
            console.dir(Router);
            expect(Router.parseParameters("")).toEqual(new Map());
        });
        test("returns an object when the query is one string", () => {
            const expected = new Map();
            expected.set("scientists", undefined);
            expect(Router.parseParameters("scientists")).toEqual(expected);
        });
        test("returns correctly with 1 key-value pair", () => {
            const expected = new Map();
            expected.set("scientists", "seven");
            expect(Router.parseParameters("scientists=seven")).toEqual(
                expected
            );
        });
        test("returns correctly with 1 key-value pair where the key is empty", () => {
            const expected = new Map();
            expected.set("", "seven");
            expect(Router.parseParameters("=seven")).toEqual(expected);
        });
        test("returns correctly with 1 key-value pair where the value include an equal sign", () => {
            const expected = new Map();
            expected.set("scientists", "seven=scientists");
            expect(
                Router.parseParameters("scientists=seven=scientists")
            ).toEqual(expected);
        });
        test("returns 1 value, if a key is reassigned", () => {
            const expected = new Map();
            expected.set("seven", "scientists");
            expect(
                Router.parseParameters("seven=geese&seven=scientists")
            ).toEqual(expected);
        });
        test("returns correctly with 2 key-value pair", () => {
            const expected = new Map();
            expected.set("scientists", "seven");
            expected.set("ki", "helpful");
            expect(
                Router.parseParameters("scientists=seven&ki=helpful")
            ).toEqual(expected);
        });
        test("returns correctly with 2 key-value pairs where the last key is empty", () => {
            const expected = new Map();
            expected.set("berlin", "cool");
            expected.set("", "seven");
            expect(Router.parseParameters("berlin=cool&=seven")).toEqual(
                expected
            );
        });
        describe("with the key prototype", () => {
            const input = "prototype=science";
            test("returns the correct Map", () => {
                const expected = new Map();
                expected.set("prototype", "science");
                const result = Router.parseParameters(input);
                expect(result).toEqual(expected);
            });
            test("does not allow to pollute the prototype of Map", () => {
                const mapPrototype = Map.prototype;
                Router.parseParameters("prototype=science");
                expect(Map.prototype).toBe(mapPrototype);
            });
            test("does not allow to pollute the prototype of Object", () => {
                const objectPrototype = Object.prototype;
                Router.parseParameters("prototype=science");
                expect(Object.prototype).toBe(objectPrototype);
            });
        });
    });
    describe("extractPathAndParams", () => {
        test("for a path with no query", () => {
            const input = "/search";
            expect(Router.extractPathAndParams(input)).toEqual([
                "/search",
                "",
                new Map()
            ]);
        });
        test("for a path with an empty query", () => {
            const input = "/search?";
            expect(Router.extractPathAndParams(input)).toEqual([
                "/search",
                "",
                new Map()
            ]);
        });
        test("for a path with one parameter", () => {
            const input = "/search?query=berlin";
            expect(Router.extractPathAndParams(input)).toEqual([
                "/search",
                "query=berlin",
                new Map().set("query", "berlin")
            ]);
        });
        test("for a path with two question marks", () => {
            const input = "/search?query=berlin?breaks=no";
            expect(Router.extractPathAndParams(input)).toEqual([
                "/search",
                "query=berlin?breaks=no",
                new Map().set("query", "berlin?breaks=no")
            ]);
        });
    });
    describe("with 1 route and 2 default routes", () => {
        const history = {
            currentUrl: "/",
            watch: () => {}
        };
        const routes = new Map([
            [
                "logout",
                {
                    url: "/logout",
                    handler: jest.fn()
                }
            ],
            [
                "default1",
                {
                    handler: jest.fn()
                }
            ],
            [
                "default2",
                {
                    handler: jest.fn()
                }
            ]
        ]);
        const router = new Router(history);
        router.init(routes);
        test("calls the correct route when there is no parameter", () => {
            router.handle("/logout");
            expect(routes.get("logout").handler).toHaveBeenCalledTimes(1);
            expect(routes.get("default1").handler).toHaveBeenCalledTimes(0);
            expect(routes.get("default2").handler).toHaveBeenCalledTimes(0);
        });
        test("calls the correct route when there is a parameter", () => {
            router.handle("/logout?cache=clear");
            expect(routes.get("logout").handler).toHaveBeenCalledTimes(1);
            expect(routes.get("default1").handler).toHaveBeenCalledTimes(0);
            expect(routes.get("default2").handler).toHaveBeenCalledTimes(0);
        });
        test("calls the last default when no route matches", () => {
            router.handle("/does-not-exist");
            expect(routes.get("logout").handler).toHaveBeenCalledTimes(0);
            expect(routes.get("default1").handler).toHaveBeenCalledTimes(0);
            expect(routes.get("default2").handler).toHaveBeenCalledTimes(1);
        });
    });
});
