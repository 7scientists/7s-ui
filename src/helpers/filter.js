export default class FilterBaseHelper {
    static applyFilter(data, filterTerm, applyOn) {
        if (
            filterTerm === undefined ||
            filterTerm === "" ||
            filterTerm.length < 1
        )
            return data;

        if (Array.isArray(applyOn)) {
            let result = applyOn.map(field =>
                this.filter(data, filterTerm, field)
            );
            result = result.flat();
            return result.filter(
                (value, index, self) => self.indexOf(value) === index
            );
        }

        return this.filter(data, filterTerm, applyOn);
    }

    static filter(data, term, field) {
        return data.filter(item =>
            item[field].toLowerCase().includes(term.trim().toLowerCase())
        );
    }
}
