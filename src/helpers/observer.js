export default class Observer {
    constructor() {
        this.watchers = new Map([]);
        this.watcherId = 0;
    }

    watch(watcher) {
        this.watchers.set(this.watcherId, watcher);
        return this.watcherId++;
    }

    unwatch(watcherId) {
        if (!this.watchers.has(watcherId)) throw "watcher does not exist";
        this.watchers.delete(watcherId);
    }

    notify(...args) {
        this.watchers.forEach((watcher, watcherId) => {
            if (this.watchers.has(watcherId)) {
                watcher(this, ...args);
            }
        });
    }
}
