import { ASC, DESC } from "../actions/sort";

export default class SortBaseHelper {
    static applySort(data, sortField, sortOrder) {
        if (sortField === undefined) return data;

        if (sortOrder === undefined) sortOrder = ASC;

        return data.sort((a, b) => {
            const fieldA = a[sortField].toLowerCase();
            const fieldB = b[sortField].toLowerCase();

            switch (sortOrder) {
                case ASC:
                    if (fieldA < fieldB) return -1;
                    if (fieldA > fieldB) return 1;
                    return 0;
                case DESC:
                    if (fieldA < fieldB) return 1;
                    if (fieldA > fieldB) return -1;
                    return 0;
            }
        });
    }
}
