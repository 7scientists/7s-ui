export class Store {
    constructor() {
        this.watchers = new Map([]);
        this.watcherId = 0;
        this.state = {};
    }

    set(key, value, overwrite) {
        if (key === "")
            Object.keys(value).forEach(key => {
                this.state[key] = value[key];
            });
        else if (overwrite === true) {
            this.state[key] = value;
        } else {
            if (this.state[key] === undefined) this.state[key] = {};
            Object.keys(value).forEach(valueKey => {
                this.state[key][valueKey] = value[valueKey];
            });
        }
        this.notify(key, this.state[key]);
        this.notify("", this.state);
    }

    get(key) {
        if (key === "" || key === null)
            //we return the whole store
            return this.state;
        return this.state[key];
    }

    watch(key, watcher) {
        if (!this.watchers.has(key)) this.watchers.set(key, new Map([]));
        this.watchers.get(key).set(this.watcherId, watcher);
        return this.watcherId++;
    }

    unwatch(key, watcherId) {
        if (!this.watchers.has(key) || !this.watchers.get(key).has(watcherId))
            throw "unknown key";
        this.watchers.get(key).delete(watcherId);
    }

    notify(key, value) {
        if (!this.watchers.get(key)) return;
        this.watchers.get(key).forEach(watcher => {
            watcher(this, key, value);
        });
    }
}

export class LocalStorageStore extends Store {
    constructor() {
        super();
    }
}

export default Store;
