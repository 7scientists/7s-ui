import Observer from "./observer";

export class History extends Observer {}

export class BrowserHistory extends History {
    constructor() {
        super();
        window.onpopstate = event => {
            this.notify(this.currentUrl, document.title, event.state);
        };
    }

    get currentUrl() {
        return location.pathname + location.search;
    }

    replaceUrl(url, title, state) {
        history.replaceState(state, title, url);
        this.notify(this.currentUrl, title, state);
    }

    pushUrl(url, title, state) {
        if (url === this.currentUrl) history.replaceState(state, title, url);
        else history.pushState(state, title, url);
        this.notify(this.currentUrl, title, state);
    }
}

export class ElectronHistory extends History {
    constructor() {
        super();
        this.history = [];
        this._currentUrl = "/";
    }

    get currentUrl() {
        return this._currentUrl;
    }

    replaceUrl(url, title, state) {
        if (this.currentUrl === url) return;
        this.history.push({ url: url, title: title, state: state });
        this._currentUrl = url;
        this.notify(this.currentUrl, title, state);
    }

    pushUrl(url, title, state) {
        if (this.currentUrl === url) return;
        this._currentUrl = url;
        this.notify(this.currentUrl, title, state);
    }
}

export class Router extends Observer {
    constructor(history) {
        super();
        this.currentRoute = undefined;
        this.currentPath = undefined;
        this.currentParams = undefined;
        this.currentUrl = undefined;
        this.routesByName = {};
        this.history = history;
    }

    static parseParameters(query) {
        const result = new Map();
        query.split("&").forEach(function(part) {
            const separatorIndex = part.indexOf("=");
            let key, value;
            if (separatorIndex === -1) {
                key = part;
            } else {
                key = part.substring(0, separatorIndex);
                value = decodeURIComponent(part.substring(separatorIndex + 1));
            }
            if (key || value) {
                result.set(key, value);
            }
        });
        return result;
    }

    static extractPathAndParams(url) {
        let path, query, params;
        const i = url.indexOf("?");
        if (i !== -1) {
            path = url.substring(0, i);
            query = url.substring(i + 1);
            params = Router.parseParameters(query);
        } else {
            path = url;
            query = "";
            params = new Map();
        }
        return [path, query, params];
    }

    handle(url) {
        const [path, query, params] = Router.extractPathAndParams(url);
        let matchingRoute;
        let defaultRoute;
        let found = false;
        for (const [, route] of this.routes) {
            if (route.url === undefined) {
                //this is the default handler
                defaultRoute = route;
            }
            const regex = new RegExp(`^${route.url}$`);
            const result = regex.exec(path);
            if (result !== null) {
                matchingRoute = route.handler.bind(this)(
                    ...result.slice(1),
                    params,
                    path,
                    query
                );
                found = true;
                break;
            }
        }

        if (!found) {
            matchingRoute = defaultRoute.handler.bind(this)(params, path);
        }

        if (matchingRoute !== undefined) {
            this.currentRoute = matchingRoute;
            this.currentParams = params;
            this.currentPath = path;
            this.currentUrl = url;
            return matchingRoute;
        }

        return undefined;
    }

    update(history, url) {
        const route = this.handle(url);
        if (route !== undefined) this.notify(route, url);
    }

    init(routes) {
        this.routes = routes;
        this.history.watch(this.update.bind(this));
        this.update(this.history, this.history.currentUrl);
    }

    // TODO: Implement navigateToView
    // navigateToView(view, ...args){
    //     this.history.pushUrl(url, '', {});
    // }

    replaceUrl(url) {
        this.history.replaceUrl(url, "", {});
    }

    navigateToUrl(url) {
        this.history.pushUrl(url, "", {});
    }
}
