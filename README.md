# 7scientists - React UI Toolkit

This repository contains reusable UI components and helpers that we use in all
of our React/Electron apps. This package can be installed using `npm`.

## Installation

Simply install the package using NPM:

```bash
npm install --save git+ssh://git@gitlab.com:7scientists/7s-ui.git
```

Alternatively, you may clone the git repository and manually symlink it
to a `7s` subdirectory in your `node_modules` directory, which is more
convenient when making changes to the toolkit:

```
git clone git@gitlab.com:7scientists/7s-ui.git
ln -s [location-of-7s-ui] node_modules/7s
```

## Usage

All JS/JSX modules are located in the `src` folder, to use them via Webpack.
Therefore, you need to define an alias in your Webpack config:

```javascript
var config = {
    resolve : {
        alias: {
            '7s' : '7s/src',
        }
    }
}
```

## Development Strategy

Most of the functionality of the app should reside in `generic` and make use of abstract interfaces to speak to the external world, where needed (e.g. for resource loading). Additional functionality (e.g. for the desktop version) should be injected into the generic components via a hooks / provider scheme, where components receive a `settings` instance that might contain various functions that implement or override certain functionalities (e.g. for loading files).

## Contributing

To learn more about development processes and submitting code, please read [Contributing.md](./Contributing.md).
