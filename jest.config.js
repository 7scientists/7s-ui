// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html
/* eslint-env node */

module.exports = {
    // Automatically clear mock calls and instances between every test
    clearMocks: true,
    // The directory where Jest should output its coverage files
    coverageDirectory: "public/coverage",
    // The glob patterns Jest uses to detect test files
    testMatch: ["**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec).[tj]s?(x)"]
};
