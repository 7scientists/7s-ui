// This is the configuration file for react-styleguidist
/* eslint-env node */
module.exports = {
    components: "src/components/**/*.{jsx,tsx}",
    styleguideDir: "public/styleguide",
    title: "7scientists React Components"
};
