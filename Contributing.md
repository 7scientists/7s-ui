# Contributing to 7scientists - React UI Toolkit

Thanks a lot for your interest to contribute to this repository!

##  Development Guidelines

The source tree in `src` is split into three subdirectory:

* `generic` contains components and libraries that are relevant both for the web and the desktop version of the software.
* `web` contains components and libraries that are relevant only to the web application.
* `desktop` contains components and libraries that are relevant only to the desktop application.

As a rule of thumb, `web` and `generic` should never make any reference to electron-specific functionality (such as importing `electron`), and `desktop` and `generic` should never make any reference to web-specific functionality.

## Development Setup

This guide is supposed to help you to contribute code.

### Git Hooks

This repository uses [Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) via [Husky](https://github.com/typicode/husky#readme).

To disable it, run:
```sh
HUSKY_SKIP_INSTALL=1 npm install
```

During a rebase you may want to skip all hooks, you can use HUSKY_SKIP_HOOKS environment variable.
```sh
HUSKY_SKIP_HOOKS=1 git rebase ...
```
